import datetime
import shutil
import os
import glob
date=datetime.datetime.now().strftime("%Y_%m_%d")
dtime=datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
result="./Report/Result_"+date
output=result+"/output_"+dtime
screenhot=output+"/screenshot"
os.makedirs(result,exist_ok=True)
os.makedirs(output,exist_ok=True)
os.makedirs(screenhot,exist_ok=True)
try:
    shutil.move("./log.html",output+"/log.html")
    shutil.move("./report.html",output+"/report.html")
    shutil.move("./output.xml",output+"/log.xml")
except:
    print("no files")
source = './'
mydict = {
    './': ['jpg','png','gif'],

}
for destination, extensions in mydict.items():
    for ext in extensions:
        for file in glob.glob(source + '*.' + ext):
            shutil.move(file, screenhot)