*** Settings ***
Library  Selenium2Library
Library  AllureLibrary
Resource  ../Resources/Keywords.robot
Variables  ../JSON/Locators.py
Variables  ../TestData/TestData.py
Test Setup  Open the browser and enter the url
TEST TEARDOWN  close the browsers
Test Template  Invalid scenario

*** Variables ***


*** Test Cases ***  username  password
case1  abc@gmail.com  skjdksdj
case2  defgg@gmail.com  skjdksdj
case3  hijk@gmail.com  skjdksdj
#Login with excel using  ${username}  {password}

*** Keywords ***
Invalid scenario
    [Arguments]  ${username}  ${password}
    Login to Application  ${username}  ${password}

