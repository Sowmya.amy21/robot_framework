*** Settings ***
Library  Selenium2Library  timeout=10   implicit_wait=1.5   run_on_failure=Capture Page Screenshot
Library  AllureLibrary
Resource  ../Resources/Keywords.robot
Variables  ../JSON/Locators.py
Variables  ../TestData/TestData.py
Suite Teardown  Close the Browsers

*** Variables ***

*** Keywords ***

*** Test Cases ***
First Test Case
    [Tags]  smoke
    Open the browser and enter the url
    Login to Application  ${username}  ${password}
    close all browsers