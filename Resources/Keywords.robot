from allure.constants import AttachmentType

*** Settings ***
Library  Selenium2Library
Library  AllureLibrary
Library  ../JSON/Locators.py
Variables  ../JSON/Locators.py
*** Variables ***
${URL}  https://admin-demo.nopcommerce.com/login?ReturnUrl=%2Fadmin%2F
${Browser}  Chrome

*** Keywords ***
Open the browser and enter the url
    open browser  ${URL}  ${Browser}

Close the Browsers
    close all browsers

Login to Application
    [Arguments]  ${username}  ${password}
    input text  ${usernameTxtbox}  ${username}
    input text  ${passwordtxtbox}  ${password}
    click button  ${login_btn}
    sleep  3s

Default Teardown
    ${test}  get keyword names
    log to console  ${test}
    Run Keyword If Test Failed    Capture Page Screenshot
    close all browsers

