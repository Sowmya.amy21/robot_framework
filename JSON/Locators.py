import json
import jsonpath

def readLocatorsFromJson(locatorname):
    try:
        openFile=open("../JSON/Locators.json")
    except:
        openFile = open("JSON\Locators.json")
    response=json.loads(openFile.read())
    value=jsonpath.jsonpath(response,locatorname)
    return value[0]

#Initialize the JSON Value to variables
usernameTxtbox = readLocatorsFromJson("Login_Page.username_txtbox")
passwordtxtbox= readLocatorsFromJson("Login_Page.password_txtbox")
login_btn= readLocatorsFromJson("Login_Page.login_btn")